# Fat Cat Simulator (Feed the cat sim)#

### What's this? ###

* A quick and silly game designer by my brother and I, in 3 days (or more ;) )
* Version **0.2** updates textures. You didn't see update 0.1 so don't worry!

### How to play ###

You are the cat. You are hungry, and you already have food to eat. Just by chance though, your dog friends are eating at the same time as you! Your goal is to eat as much of their food as possible, and then come back to your food. Hint: some dogs eat faster than others!

Download [love2d 0.9.1](https://bitbucket.org/rude/love/downloads) for your system and drag the fatcat.love file on the executable!

### Some screenshots ###
![Game Screen](https://bitbucket.org/repo/xjz7R7/images/2910096270-cat1.png)

*Written by P. and M. Bantolas.*