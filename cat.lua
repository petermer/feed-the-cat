Class = require 'hump.class'

Cat = Class{
	init = function(self, posx, posy)
		self.posx = posx
		self.posy = posy
		self.vel = {
			x = 0,
			y = 0
		}
		self.force = 40
		self.mass = 2
		self.eatSpeed = 7

		self.foodEaten = 0
		self.canEat = false
		self.target = nil
	end
}

function Cat:reset(x, y)
	self.posx = x
	self.posy = y
	self.vel.x = 0
	self.vel.y = 0
	self.mass = 2
	self.eatSpeed = 7

	self.foodEaten = 0
	self.canEat = false
	self.target = nil
	self.eating = false
end

function Cat:update(dt)
	local moving = false

	if love.keyboard.isDown('a') then
		self.vel.x = self.vel.x - self.force / self.mass * dt
	elseif love.keyboard.isDown('d') then
		self.vel.x = self.vel.x + self.force / self.mass * dt
	end

	self.vel.x = 0.96 * self.vel.x
	if math.abs(self.vel.x) < 0.12 then
		self.vel.x = 0
	else
		moving = true
	end

	self.posx = self.posx + self.vel.x

	if love.keyboard.isDown('w') then
		self.vel.y = self.vel.y - self.force / self.mass * dt
	elseif love.keyboard.isDown('s') then
		self.vel.y = self.vel.y + self.force / self.mass * dt
	end
	
	self.vel.y = 0.96 * self.vel.y
	if math.abs(self.vel.y) < 0.12 then
		self.vel.y = 0
	else
		moving = true
	end

	self.posy = self.posy + self.vel.y

	if moving then
		local expVol = (math.exp(FCS.settings.sound / 100) - 1) / (math.exp(1) - 1)
		FCS.sounds['walk']:setVolume(expVol)
		FCS.sounds['walk']:play()
	else
		FCS.sounds['walk']:pause()
	end

	if love.keyboard.isDown(' ') and self.canEat then
		if self.target.rem > 0 then
			self.eating = true
			local justAte = math.min(self.eatSpeed * dt, self.target.rem)
			self.target.rem = self.target.rem - justAte
			self.foodEaten = self.foodEaten + justAte

			-- adjust rate
			self.eatSpeed = math.exp(-0.003 * self.foodEaten) * 7

			self.mass = self.mass + justAte * 0.02

			local expVol = (math.exp(FCS.settings.sound / 100) - 1) / (math.exp(1) - 1)
			FCS.sounds['eat']:setVolume(expVol)
			FCS.sounds['eat']:play()
		else
			FCS.sounds['eat']:pause()
		end
	else
		FCS.sounds['eat']:pause()
		self.eating = false
	end

	self.canEat = false
end

function Cat:draw()
	love.graphics.draw(cat_image, self.posx, self.posy)
end

function Cat:die()
	FCS.sounds['walk']:stop()
	FCS.sounds['eat']:stop()
end