function love.conf(t)
	t.identity = 'fcs'

	t.window.width = 600
	t.window.height = 400
	t.window.title = 'Fat Cat Simulator'
	t.window.icon = 'assets/icon.png'
	-- t.window.vsync = false
end