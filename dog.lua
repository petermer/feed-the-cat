Class = require 'hump.class'

Dog = Class{
	init = function(self, food, speed)
		self.speed = speed
		self.food = food
		self.img = love.graphics.newImage('assets/dog.png')
	end
}

function Dog:update(dt)
	local justAte = math.min(self.food.rem, self.speed * dt)
	self.food.rem = self.food.rem - justAte
end

function Dog:draw()

	if self.speed == 0 then
		return
	end

	local posx = self.food.x
	local posy = self.food.y
	love.graphics.draw(self.img, posx + 60, posy)
end