Class = require('hump.class')

Food = Class{
	init = function(self, x, y, img)
		self.x = x
		self.y = y
		self.img = img
		self.rem = 150 - math.random() * 25

		self.cs = Collider:addRectangle(x, y, 64, 32)
		self.cs.type = 'food'
		self.cs.parent = self
	end
}

function Food:draw()
	love.graphics.draw(self.img, self.x, self.y)
end