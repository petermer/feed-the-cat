require 'cat'
require 'dog'
require 'food'
HC = require 'hardoncollider'

BLACK_COL = {0, 0, 0, 255}
CLEAR_COL = {255, 255, 255, 255}

_DEBUG = false

GameState = {}

function on_collide(dt, shape_a, shape_b)
	local cat_shape, target_shape
	if shape_a.type == 'food' then
		cat_shape = shape_b
		target_shape = shape_a
	elseif shape_b.type == 'food' then
		cat_shape = shape_a
		target_shape = shape_b
	end

	cat.canEat = true
	cat.target = target_shape.parent

end

-- Food table
GameState['foods'] = {}

-- global timer
GameState.remaining_time = 40
GameState.finished = false

function GameState:init()

	catfood = love.graphics.newImage('assets/catfood.png')
	dogfood1 = love.graphics.newImage('assets/dogfood1.png')	
	dogfood2 = love.graphics.newImage('assets/dogfood2.png')
	dogfood3 = love.graphics.newImage('assets/dogfood3.png')
	help = love.graphics.newImage('assets/help.png')
	
	local images = {
		[1] = catfood,
		[2] = dogfood1,
		[3] = dogfood2,
		[4] = dogfood3
	}

	cat_image = love.graphics.newImage('assets/cat.png')

	cat = Cat(5, 200)
	dogs = {}

	Collider = HC(100, on_collide)
	cat_collider = Collider:addCircle(cat.posx + 128, cat.posy + 64, 25)
	cat_collider.type = 'cat'

	-- init foods
	for j,im in pairs(images) do
		GameState.foods[j] = Food(j * 120 - 30, 50, im)

		if im ~= catfood then
			local eatSpeed = math.floor(math.random() * 5 + 3)
			dogs[j] = Dog(GameState.foods[j], eatSpeed)
		end
	end

	-- init main canvas
	mainCanvas = love.graphics.newCanvas()
end

function GameState:enter()

	sceneAlpha = 255

	GameState.finished = false

	-- set background color
	love.graphics.setBackgroundColor(242, 238, 225)

	cat.posx = 5
	cat.posy = 200
	cat:reset(5, 200)

	for j, f in pairs(GameState.foods) do
		f.rem = 150 - math.random() * 25
	end

	for j, d in pairs(dogs) do
		d.speed = math.floor(math.random() * 5 + 3)
	end
end

function GameState:update(t)
	sceneAlpha = sceneAlpha - math.min(sceneAlpha, 255/2*t)

	if GameState.finished then
		-- love.audio.stop()
		return
	end

	local time_passed = math.min(t, GameState.remaining_time)
	GameState.remaining_time = GameState.remaining_time - time_passed
	if GameState.remaining_time <= 0 then
		GameState.finished = true
		cat:die()
	end

	cat:update(t)
	cat_collider:moveTo(cat.posx + 110, cat.posy + 20)

	for i, d in pairs(dogs) do
		d:update(t)
	end

	Collider:update(dt)
end

function GameState:draw()

	love.graphics.setCanvas(mainCanvas)
	-- mainCanvas:clear(255, 255, 255, 255)
	mainCanvas:clear()

	if cat.eating then
		love.graphics.translate(math.random() * 8, 0)
	end

	love.graphics.draw(help, 0, love.graphics.getHeight() - 100)

	for i, f in pairs(GameState.foods) do
		f:draw()
		
		-- print left amount
		love.graphics.setColor(BLACK_COL)
		love.graphics.print('left: ' .. math.floor(f.rem), f.x, f.y - 20)

		if _DEBUG then
			f.cs:draw()
		end
		
		love.graphics.setColor(CLEAR_COL)
	end

	for i, d in pairs(dogs) do
		local f = d.food

		d:draw()
		love.graphics.setColor(BLACK_COL)

		if _DEBUG then
			love.graphics.print('rate: ' .. math.floor(d.speed), f.x, f.y - 40)
		end

		love.graphics.setColor(CLEAR_COL)
	end

	cat:draw()

	local r,g,b = love.graphics.getColor()
	love.graphics.setColor(0,0,255)
	
	if _DEBUG then
		cat_collider:draw('line')
	end

	love.graphics.setColor(0, 0, 0)
	love.graphics.print('cat: ' .. math.floor(cat.foodEaten), 10, 10)
	if _DEBUG then
		love.graphics.print(string.format('rate: %.2f', cat.eatSpeed), cat.posx, cat.posy - 17)
		love.graphics.print(string.format('mass: %.2f', cat.mass), cat.posx, cat.posy)
		love.graphics.print('FPS: '..tostring(love.timer.getFPS()), 500, 10)
	end

	-- print time
	love.graphics.print(string.format("Time %02d:%02d", math.floor(GameState.remaining_time / 60), math.floor(GameState.remaining_time) % 60), 510, love.graphics.getHeight() - 25, 0, 1.3, 1.3, 10, 7)
	love.graphics.setColor(r,g,b)


	if GameState.finished then
		local finished_text = 'FINISHED!!1'
		local tw = fonts.large:getWidth(finished_text)
		local noise_factor = math.sin(love.timer.getTime() * 0.89) * 0.2
		love.graphics.setColor(BLACK_COL)
		love.graphics.translate(love.graphics.getWidth()/2, love.graphics.getHeight()/2)
		love.graphics.rotate(math.pi/17 * math.sin(love.timer.getTime() * 0.67))
		love.graphics.scale(2 + noise_factor, 2 + noise_factor)
		love.graphics.setFont(fonts.large)
		love.graphics.print('FINISHED!!1', -tw/2, -16)

		love.graphics.print('Score - '..math.floor(cat.foodEaten), -tw/2, 0)
		love.graphics.print('Press Enter', -tw/2, 16)
		love.graphics.setColor(CLEAR_COL)
		love.graphics.setFont(fonts.normal)
	end

	love.graphics.setCanvas()
	love.graphics.origin()

	love.graphics.setColor(255, 255, 255, 255 - sceneAlpha)
	love.graphics.draw(mainCanvas, 0, 0)

	love.graphics.setColor(CLEAR_COL)
end

function GameState:leave()
	cat:die()
end

function GameState:keyreleased(key, code)
	if key == 'rctrl' then
		_DEBUG = not _DEBUG
	end

    if key == 'end' then
        GameState.remaining_time = 1
    end

    if key == 'pagedown' then
        cat.foodEaten = cat.foodEaten + 100
    end

	if GameState.finished and key == 'return' then

        -- save high score
        FCS.highscores.add(GameState.mode, math.floor(cat.foodEaten))

		StateMgr.switch(MenuState)
	end

	if key == 'escape' then
		love.event.quit()
	end

end
