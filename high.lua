HighScoresState = {}

function HighScoresState:draw()
    love.graphics.setBackgroundColor(0,0,0,255)

    local noScores = true
    local w, h = love.window.getDimensions()

    love.graphics.draw(FCS.img.sky, 0, 0)

    love.graphics.setFont(fonts.large)

    love.graphics.print('High Scores', 10, 10)

    local tplWidth = fonts.large:getWidth('1. asd - 000')
    for i, score, name in FCS.highscores() do
        noScores = false
        love.graphics.print(i ..'. ' .. name .. ' - ' .. score, w/2 - tplWidth/2, h/2 - 5 * 20 + i * 20)
    end

    local tw = fonts.large:getWidth('No scores!')
    if noScores then
        love.graphics.print('No scores!', w/2 - tw/2, h/2 - 10)
    end

    love.graphics.setFont(fonts.normal)
    love.graphics.print('Backspace Back', 10, h - 20)
end

function HighScoresState:keyreleased(key, code)
    if key == 'escape' then
        love.event.quit()
    end

    if key == 'backspace' then
        StateMgr.switch(MenuState)
        FCS.sounds.click:play()
    end
end
