StateMgr = require 'hump.gamestate'
require 'menu'
require 'game'
local sick = require 'libs/sick'

function love.load()
	-- seed RNG
	math.randomseed( os.time() )

	love.graphics.setDefaultFilter('nearest', 'nearest')
	
    --[[
	local _charmap=" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
	FONT = love.graphics.newImageFont('assets/font.png', _charmap)
	love.graphics.setFont(FONT)
    --]]

	fonts = {
		normal = love.graphics.newFont('assets/visitor.ttf', 14),
        medium = love.graphics.newFont('assets/visitor.ttf', 16),
		large = love.graphics.newFont('assets/visitor.ttf', 20)
	}
	love.graphics.setFont(fonts.normal)

    FCS = {
        tutorial = false,
        music = love.audio.newSource('assets/sound/music.mp3'),
        sounds = {
        	eat = love.audio.newSource('assets/sound/eat.wav', 'static'),
        	walk = love.audio.newSource('assets/sound/move_sound.mp3', 'static')
    	},

    	img = {
    		sky = love.graphics.newImage('assets/sky.png'),
    		grass = love.graphics.newImage('assets/grass.png')
    	},

        settings = {
        	music = 40,
        	sound = 80
        }
    }

    -- handle highscores
    local highfile = 'highscores.lst'
    sick.set(highfile, 5, '???', 0)

    FCS.highscores = sick

    FCS.sounds.click = love.audio.newSource('assets/sound/click.ogg', 'static')

    local expVol = (math.exp(FCS.settings.music / 100) - 1) / (math.exp(1) - 1)
	FCS.music:setVolume(expVol)
    FCS.music:setLooping(true)
    FCS.music:play()

	StateMgr.registerEvents()
	StateMgr.switch(MenuState)
end

function love.quit()
    FCS.highscores.save()
end
