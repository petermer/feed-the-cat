require 'game'
require 'high'
require 'tutorial'
local anim8 = require 'libs/anim8'

MenuState = {}

local nextState

function MenuState:init()
	sprite = love.graphics.newImage('assets/cat_sprite.png')
	local g = anim8.newGrid(44, 34, sprite:getWidth(), sprite:getHeight())
	animation = anim8.newAnimation(g('1-6', 2), 0.2)
end

function MenuState:enter()
    if FCS.tutorial then
        nextState = GameState
    else
        nextState = TutorialState
    end
end

function MenuState:update(dt)
	animation:update(dt)
end

function MenuState:draw()
	local w, h = love.graphics.getDimensions()

	-- love.graphics.setBackgroundColor({255,255,255,255})
	love.graphics.draw(FCS.img.sky, 0, 0)

	love.graphics.setBackgroundColor(0, 0, 0)

	love.graphics.setColor(255,255,255,255)
	local title = 'Fat Cat Simulator'
	local tWidth = fonts.large:getWidth(title)
    love.graphics.setFont(fonts.large)
	love.graphics.print(title, w/2, 30, 0, 1.3, 1.3, tWidth/2, 7)

	animation:draw(sprite, 100, 10)

    love.graphics.setFont(fonts.medium)
	local introText = 'It is eating time for Gklounis the Cat and his friends, the dogs. But Gklounis is greedy and wants to eat from the dogs\' foods too! Try to eat as much food as possible within the time limit!'
	love.graphics.printf(introText, w/2 - 200, 50, 400)

	local medianWidth = fonts.large:getWidth('[2] Medium')
	local scaleNoise = math.cos(0.67 * love.timer.getTime()) * 0.14
    love.graphics.setFont(fonts.large)
	love.graphics.translate(w/2, h/2)
	love.graphics.scale(2 + scaleNoise, 2 + scaleNoise)
	love.graphics.rotate(0.1)
	love.graphics.print('[1] Easy', -medianWidth/2, -20)
	love.graphics.print('[2] Medium', -medianWidth/2, 0)
	love.graphics.print('[3] Hard', -medianWidth/2, 20)

    love.graphics.origin()
    love.graphics.setFont(fonts.normal)
    love.graphics.print('H Highscores', 10, h - 20)

    local tW = fonts.normal:getWidth('Music: xxx')
    love.graphics.print('Music: '.. (FCS.settings.music), w - tW - 10, h - 35)
    local tW = fonts.normal:getWidth('Sound: xxx')
    love.graphics.print('Sound: '.. (FCS.settings.sound), w - tW - 10, h - 20)
end

function MenuState:keypressed(key)
	local expVol = (math.exp(FCS.settings.sound / 100) - 1) / (math.exp(1) - 1)
	FCS.sounds.click:setVolume(expVol)
	FCS.sounds.click:play()
end

function MenuState:keyreleased(key, code)
	if key == '1' then
        GameState.mode = 'EZY'
		GameState.remaining_time = 45
		StateMgr.switch(nextState)
	elseif key == '2' then
        GameState.mode = 'MED'
		GameState.remaining_time = 30
		StateMgr.switch(nextState)
	elseif key == '3' then
        GameState.mode = 'HRD'
		GameState.remaining_time = 20
		StateMgr.switch(nextState)
	end

    if key == 'h' then
        StateMgr.switch(HighScoresState)
    end

	if key == 'escape' then
		love.event.quit()
	end

	-- set volume
	local cVol = FCS.settings.music
	if key == 'up' then

		if love.keyboard.isDown('lshift') then
			-- adjust sound volume
			FCS.settings.sound = math.min(FCS.settings.sound + 10, 100)
		else
			FCS.settings.music = math.min(cVol + 10, 100)
			local expVol = (math.exp(FCS.settings.music / 100) - 1) / (math.exp(1) - 1)
			FCS.music:setVolume(expVol)
		end
	elseif key == 'down' then

		if love.keyboard.isDown('lshift') then
			-- adjust sound volume
			FCS.settings.sound = math.max(FCS.settings.sound - 10, 0)
		else
			FCS.settings.music = math.max(cVol - 10, 0)
			local expVol = (math.exp(FCS.settings.music / 100) - 1) / (math.exp(1) - 1)
			FCS.music:setVolume(expVol)
		end
	end

	if key == 'm' then
		FCS.music:setVolume(0)
	end
end
