#!/bin/sh

tmpDir=$(mktemp -d)
origDir=$(pwd)

cp -R *.lua assets hardoncollider hump libs $tmpDir
rm -rf "$tmpDir/assets/res"

cd $tmpDir

zip -r ../fatcat.love *
mv ../fatcat.love $origDir

cd $origDir
