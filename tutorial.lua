TutorialState = {}

local tutorialImage
local timeout = 6
function TutorialState:init()
    tutorialImage = love.graphics.newImage('assets/tutorial.png')
    
    FCS.tutorial = true
end

function TutorialState:update(dt)
    local dtAdjusted = math.min(dt, timeout)

    timeout = timeout - dtAdjusted

    if timeout <= 0 then
        StateMgr.switch(GameState)
    end
end

function TutorialState:draw()
    love.graphics.draw(tutorialImage, 0, 0)
end

function TutorialState:keyreleased(key, code)
    if key == ' ' then
        StateMgr.switch(GameState)
    end
end
